﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace asptest.Services
{
    public interface IDateTimeHandler
    {
        string ConvertDateTimeToString(DateTime datetime);
    }
}
