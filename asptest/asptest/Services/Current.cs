﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace asptest.Services
{
    public class Current : IDateTimeHandler
    {
        string IDateTimeHandler.ConvertDateTimeToString(DateTime datetime)
        {
            return DateTime.Now.ToString("t") + " Current";
        }
    }
}
