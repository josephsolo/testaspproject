﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace asptest.Services
{
    public class Utc : IDateTimeHandler
    {
        string IDateTimeHandler.ConvertDateTimeToString(DateTime datetime)
        {
            return DateTime.UtcNow.ToString("t") + " UTC";
        }
    }
}
