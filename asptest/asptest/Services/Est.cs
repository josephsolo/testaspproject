﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace asptest.Services
{
    public class Est : IDateTimeHandler
    {
        string IDateTimeHandler.ConvertDateTimeToString(DateTime datetime)
        {
            var timeUtc = DateTime.UtcNow;
            TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, easternZone);

            return easternTime.ToString("t") + " EST";
        }
    }
}
