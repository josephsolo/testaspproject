﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using asptest.Models;

using System.Text.Json;

namespace asptest.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View("About");
        }

        public IActionResult Hello(string name)
        {
            ViewData["Message"] = name;
            return View("Hello");
        }

        public IActionResult GetData()
        {
            string json = System.IO.File.ReadAllText(@"Data\sample.json");
            List<Models.Person> people = JsonSerializer.Deserialize<List<Models.Person>>(json);

            return View(people);
        }
    }
}
