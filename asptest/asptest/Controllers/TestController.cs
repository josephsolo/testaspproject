﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using asptest.Services;

namespace asptest.Controllers
{
    public class TestController : Controller
    {
        IDateTimeHandler dateTime;

        public TestController(IDateTimeHandler dateTimeHandler)
        {
            dateTime = dateTimeHandler;
        }

        public IActionResult Index()
        {
            return View();
        }

        public string GetString()
        {
            return "Hello";
        }

        public string Hello(string name)
        {
            return "Hello, " + name;
        }

        public int GetDayOfYear()
        {
            return DateTime.Now.DayOfYear;
        }

        public string GetTime()
        {
            return dateTime.ConvertDateTimeToString(DateTime.Now);

            //int tod = DateTime.Now.Hour;
            //if (tod >= 22 && tod < 4)
            //    return DateTime.Now.TimeOfDay.ToString() + ", Night";
            //else if (tod >= 4 && tod < 12)
            //    return DateTime.Now.TimeOfDay.ToString() + ", Morning";
            //else if (tod >= 12 && tod < 16)
            //    return DateTime.Now.TimeOfDay.ToString() + ", Day";
            //else
            //    return DateTime.Now.TimeOfDay.ToString() + ", Evening";
        }

        public int Sum(int num1, int num2)
        {
            return num1 + num2;
        }
    }
}